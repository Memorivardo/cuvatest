<?php

namespace App\Http\Controllers;

use App\Hotel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class HotelsController extends Controller
{
    public function index()
    {
        return response(Hotel::all()->jsonSerialize(), Response::HTTP_OK);
    }
}
