<?php

namespace App\Http\Controllers;

use App\Booking;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BookingController extends Controller
{
    public function store(Request $request)
    {

        $data = $request->validate([
            'hotel_id' => 'required|integer',
            'date_start' => 'required|date',
            'date_end' => 'required|date',
            'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
            'mail' => 'required|email',
            'surname' => 'required|string|regex:/^[\w\-\s]+$/',
            'name' => 'required|string|regex:/^[\w\-\s]+$/',
            'patronymic' => 'required|string|regex:/^[\w\-\s]+$/',
        ]);

        $booking = new Booking();
        $booking->hotel_id = $request->hotel_id;
        $booking->name = $request->name;
        $booking->surname = $request->surname;
        $booking->patronymic = $request->patronymic;
        $booking->mail = $request->mail;
        $booking->phone = $request->phone;
        $booking->guests = $request->guests;
        $booking->date_start = date('Y-m-d',strtotime($request->date_start));
        $booking->date_end = date('Y-m-d',strtotime($request->date_end));
        $booking->save();

        return response($booking->jsonSerialize(), Response::HTTP_CREATED);
    }

}
