require('./bootstrap');

window.Vue = require('vue');

import Vue from 'vue';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import Vuex from 'vuex'
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(Vuex);
Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyC3_xrg1Nog1z747WEces93s4TRPm8az-s',
        libraries: 'places',
    },
    installComponents: true
});


import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'



const store = new Vuex.Store({
    state: {
        hotel: false,
        form: {
            visibility: false
        }
    },
    getters : {
        hotel : state => {
            return state.hotel
        },
        form : state => {
            return state.form
        }
    },
    mutations: {
        set_hotel (state, hotel) {
            state.hotel = hotel;
        },
        change_form_visible (state) {
            state.form.visibility = !state.form.visibility;
        }
    },
    actions : {
        set_hotel(context, payload) {
            context.commit("set_hotel",payload);
        },
        change_form_visible(context) {
            context.commit("change_form_visible");
        }
    }
});


import App from './src/App.vue';
const app = new Vue({
    el: '#app',
    store: store,
    components: {
        App
    },
    render: h => h(App)
});
